var OFF = 0, WARN = 1, ERROR = 2

module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    semi: [WARN, "never"],
    "comma-dangle": [WARN, "never"],
  }
};
