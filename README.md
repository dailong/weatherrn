# weatherRN

Clone weather tutorial project from https://www.fullstackreact.com/react-native/

# installation

## clone repository

``git clone https://gitlab.com/long.vd/react-native-codebase.git``

## install module
``yarn install`` or ``npm i``

## install podfile
``cd ios && pod install && cd ..``

## run in ios simulator

simple ``react-native run-ios``

or run with specific simulator ``react-native run-ios --simulator="YOUR SIMULATOR"``

## run in android emulator

``yarn android`` or ``npm run android``

