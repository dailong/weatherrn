import React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

class SearchInput extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inputText: '',
      placeholder: props.placeholder
    }
  }

  handleChangeText = text => {
    this.setState({
      inputText: text
    })
  }

  handleSubmit = () => {
    const { onSubmit } = this.props
    const { inputText } = this.state

    if (inputText === '') {
      return
    }

    onSubmit(inputText)
  }

  render() {
    const { inputText, placeholder } = this.state
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          autoCorrect={false}
          placeholder={placeholder}
          onChangeText={this.handleChangeText}
          onSubmitEditing={this.handleSubmit}
          value={inputText}
        />
      </View>
    )
  }
}

// MARK: - PropTypes
SearchInput.propTypes = {
  placeholder: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
}

SearchInput.defaultProps = {
  placeholder: 'name of city'
}

// MARK: - Styles
const styles = StyleSheet.create({
  container: {
    height: 60,
    marginTop: 20,
    marginHorizontal: 40,
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'white'
  },
  textInput: {
    flex: 1
  }
})

export default SearchInput
