import { API } from '../../services/api'

const FETCH_WEATHER_BEGIN = 'FETCH_WEATHER_BEGIN'
const FETCH_WEATHER_SUCCESS = 'FETCH_WEATHER_SUCCESS'
const FETCH_WEATHER_FAILURE = 'FETCH_WEATHER_FAILURE'

const fetchWeatherBegin = () => ({
  type: FETCH_WEATHER_BEGIN
})

const fetchWeatherSuccess = weather => ({
  type: FETCH_WEATHER_SUCCESS,
  payload: weather
})

const fetchWeatherFailure = error => ({
  type: FETCH_WEATHER_FAILURE,
  payload: error
})

const fetchWeather = woeid => {
  return dispatch => {
    dispatch(fetchWeatherBegin())
    API.metaweather
      .fetchWeather(woeid)
      .then(result => {
        const { consolidated_weather } = result
        var dataWeather = consolidated_weather.shift()
        console.log('consolidated_weather: ', dataWeather)
        // const { the_temp, weather_state_name } = dataWeather
        dispatch(fetchWeatherSuccess(dataWeather))
      })
      .catch(e => {
        dispatch(fetchWeatherFailure(e))
      })
  }
}

module.exports = {
  // TYPE ACTIONS
  FETCH_WEATHER_BEGIN,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE,

  fetchWeather
}
