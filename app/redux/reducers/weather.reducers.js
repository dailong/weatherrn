import {
  FETCH_WEATHER_BEGIN,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE
} from '../actions/weather.actions'

const initialState = {
  data: {},
  loading: false,
  error: null
}

const weatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_WEATHER_BEGIN:
      console.log('FETCH WEATHER BEGIN')
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_WEATHER_SUCCESS:
      console.log('FETCH WEATHER SUCCESS')
      return {
        loading: false,
        data: action.payload
      }
    case FETCH_WEATHER_FAILURE:
      console.log('FETCH WEATHER FAILURE')
      return {
        loading: false,
        error: action.payload
      }
    default:
      return state
  }
}

export default weatherReducer
