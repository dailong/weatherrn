import { metaweather } from './metaweather'

export const request = async (url, settingReq = {}) => {
  let response = await fetch(url, settingReq)
  return await response.json()
}

export const API = {
  metaweather
}
