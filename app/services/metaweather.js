import { request } from './api'

const ORIGIN = 'https://www.metaweather.com'
const API_PATH = 'api'
const LOCATION_PATH = 'location'

const BASE_URL = `${ORIGIN}/${API_PATH}/${LOCATION_PATH}`

export const metaweather = {
  fetchLocationId: searchTerm => {
    let url = `${BASE_URL}/search/?query=${searchTerm}`
    console.log('request url:', url)
    return request(url)
  },

  fetchWeather: woeid => {
    let url = `${BASE_URL}/${woeid}`
    return request(url)
  }
}
