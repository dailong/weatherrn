import { Navigation } from 'react-native-navigation'

// MARK: - Screen ID
import { SCREENID } from './ScreenId'

// MARK: - Screen Component
import MainScreen from './main/MainScreen'
import WeatherScreen from './weather/WeatherScreen'

// MARK: - Redux
import { Provider } from 'react-redux'
import store from '../redux/store'

// Every screen component in your app must be registered with a unique name.
function registerScreens() {
  // Main Screen
  Navigation.registerComponentWithRedux(
    SCREENID.MAINSCREEN,
    () => MainScreen,
    Provider,
    store
  )
  Navigation.registerComponentWithRedux(
    SCREENID.WEATHERSCREEN,
    () => WeatherScreen,
    Provider,
    store
  )
}

module.exports = {
  registerScreens
}
