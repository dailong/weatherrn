import React from 'react'
import {
  SafeAreaView,
  KeyboardAvoidingView,
  Text,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native'

import { Navigation } from 'react-native-navigation'

import { SCREENID } from '../ScreenId'

// MARK: - Redux
import { fetchLocationId } from '../../redux/actions/location.actions'
import { connect } from 'react-redux'
import store from '../../redux/store'

// MARK: - Screens
import SearchInput from '../../components/SearchInput'

// MARK: - Utils
// import getImageForWeather from '../../modules/getImageForWeather'

class MainScreen extends React.Component {
  constructor(props) {
    super(props)
    Navigation.events().bindComponent(this)
  }

  unsubscribe = null

  locationData = {
    title: 'Lon don',
    location_type: 'City',
    woeid: 2487956,
    latt_long: '37.777119, -122.41964'
  }

  state = {
    notify: true,
    status: 'Enter name of city'
  }

  componentDidAppear() {
    this.unsubcribe = store.subscribe(this.handleUpdateLocation)
  }

  pushWeatherScreen = () => {
    this.unsubcribe()

    const { componentId: currentComponentId } = this.props
    const { title, woeid } = this.locationData

    Navigation.push(currentComponentId, {
      component: {
        name: SCREENID.WEATHERSCREEN,
        passProps: {
          woeid: woeid
        },
        options: {
          topBar: {
            title: {
              text: title
            }
          }
        }
      }
    })
  }

  handleUpdateLocation = () => {
    const { data, error } = store.getState().location

    const location = data.shift()

    if (error == null) {
      if (location == null) {
        this.setState({
          notify: true,
          status: 'Can not find any city with your query'
        })
      } else {
        this.locationData = location
      }
    }
    this.setState({
      notify: error != null ? true : false,
      status: error != null ? error.message : ''
    })
  }

  _fetchLocationId = city => {
    console.log('[GET] Query with city', city)
    this.props.fetchLocationId(city)
  }

  dismissKeyboard = () => {
    Keyboard.dismiss()
  }

  render() {
    var { loading } = this.props
    const { title } = this.locationData

    const { notify, status } = this.state

    return (
      <SafeAreaView style={styles.container}>
        <TouchableWithoutFeedback onPress={this.dismissKeyboard}>
          <KeyboardAvoidingView style={styles.container} behavior="padding">
            <StatusBar barStyle="default" />
            <ActivityIndicator
              animating={loading}
              size="large"
              color="#0000ff"
            />

            {notify && (
              <Text
                style={[styles.textStyle, styles.smallText, styles.errorText]}>
                {status}
              </Text>
            )}

            {!loading && !notify && (
              <Text
                style={[styles.textStyle, styles.largeText]}
                onPress={this.pushWeatherScreen}>
                {title}
              </Text>
            )}
            <SearchInput
              placeholder={'name of city'}
              onSubmit={this._fetchLocationId}
            />
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    )
  }
}

// MARK: - Style
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'powderblue',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  imageStyle: {
    resizeMode: 'cover'
  },

  textStyle: {
    textAlign: 'center',
    color: 'white'
    // backgroundColor: 'red'
  },

  largeText: {
    fontSize: 44
  },

  smallText: {
    fontSize: 18
  },
  errorText: {
    color: 'red'
  }
})

const mapStateToProps = state => {
  return {
    data: state.location.data,
    loading: state.location.loading,
    error: state.location.error
  }
}

const mapDispatchToProps = {
  fetchLocationId
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen)
